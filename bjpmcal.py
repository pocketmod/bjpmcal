#!/usr/bin/env python3
#
# bjpmcal --- fill a template svg calendar with actual dates and week numbers
# Copyright © 2018 Georg Drees <tuxlifan@derseegrog.org>
#
# bjpmcal is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# bjpmcal is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bjpmcal.  If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################################
import calendar
#########################################################################################
#########################################################################################
#
# EDIT ONLY THIS SECTION UNLESS YOU KNOW WHAT YOU ARE DOING

TMPLFILE = "a7tmpl.svg"

# week numbers
STYLE_WKNR   = "font-size:1.23px;font-family:arial;text-align:center;text-anchor:middle;fill:#787878"
# regular days (Mo-Fr)
STYLE_NORMAL = "font-size:1.85px;font-family:arial;text-align:center;text-anchor:middle;fill:#000000"
# Saturday + Sunday
STYLE_WKEND  = "font-size:1.85px;font-family:arial;text-align:center;text-anchor:middle;fill:#787878"
# days in other months
STYLE_OTHER  = "font-size:1.85px;font-family:arial;text-align:center;text-anchor:middle;fill:#b0b0b0"

# first element is unused due to indexing 1-12
MONTH_NAMES = ["", "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]

FIRSTWEEKDAY = calendar.MONDAY  # or calendar.SUNDAY etc. Do not use quotes as this is a python statement!

# END OF SECTION
#
#########################################################################################
#########################################################################################
#
# This program assumes that the template file contains:
#
# - a line with the string "§month_year§" which will be replaced by the month according to
#   the above MONTH_NAMES and the (4 digit) year
#
# - lines with ">wN<" where 'N' is a digit. These will be replaced by (ISO) week numbers.
#   The occurrence of these svg elements in the file must be in the sorted order w1...w6
#
# - lines with ">aN<" where 'a' is a lower case letter, except "w", and 'N' is a digit.
#   The order of these lines must be week-first and ordered by day, e.g.
#     a1
#     ...
#     a7
#     b1
#     ...
#     b7
#     etc.
#   where a is the first week (row) displayed, b is the second and so on.
#
# In addition, each line with week numbers or days to be replaced must contain a style
# attribute which will be substituted as well. e.g.:
#   style="font-size:1.85000002px;font-family:arial;text-align:center;text-anchor:middle;fill:#000000">a5</text>
# You may make the style attribute in the template file empty, e.g.:
#   style="">a5</text>
#
# It might therefore be necessary for you to manually change a template file you generated
# with inkscape (as often it puts the id attribute on the line with the actual text and the
# style attribute on the first line of the svg element, as well as to check the correct
# order of the svg elements).
#
#########################################################################################

import sys
import re
import argparse
import datetime
import itertools

line_re_w = re.compile('.*>w[0-9]<')
line_re_d = re.compile('.*>[a-vx-z][0-9]<')
calendar.setfirstweekday(FIRSTWEEKDAY)

def get_monthdata(sdate):
    """Return day, format data and weeknumbers in a triple"""
    fmt = [[STYLE_NORMAL,STYLE_NORMAL,STYLE_NORMAL,STYLE_NORMAL,STYLE_NORMAL,STYLE_WKEND,STYLE_WKEND] for weeks in range(6)]
    cur = calendar.monthcalendar(sdate.year, sdate.month)
    isowk = lambda d: datetime.date.isocalendar(d)[1]
    if len(cur) < 6:
        # append "empty" week if less than 6 weeks in month
        cur.append([0,0,0,0,0,0,0])
    if len(cur) < 6:
        # e.g. Feb 2021 with monday as firstweekday has only 4 weeks, prepend empty one, too
        # Thanks to svgcalendar.py from inkscape
        cur.reverse()
        cur.append([0,0,0,0,0,0,0])
        cur.reverse()
    wknrs = [ isowk(datetime.date(sdate.year, sdate.month, cur[w][-1])) if cur[w][-1]!=0 else 0 for w in range(6) ]
    if 0 in cur[0]:
        # find last row from previous month and fill it in
        prev_month = sdate.replace(day=1)-datetime.timedelta(1)
        other = calendar.monthcalendar(prev_month.year, prev_month.month)
        if wknrs[0] == 0:
            wknrs[0] = isowk(datetime.date(prev_month.year, prev_month.month, other[-1][1]))
        for i in range(7):
            if cur[0][i] == 0:
                cur[0][i] = other[-1][i]
                fmt[0][i] = STYLE_OTHER
    if 0 in cur[-1]:
        # find first row(s) from next month and fill it in
        next_month = sdate.replace(day=1)+datetime.timedelta(32)
        other = calendar.monthcalendar(next_month.year, next_month.month)
        idx = -1
        adn = 1  # rows: cur[-1] <==> other[0]
        if 0 in cur[-2]:
            # we need the first 2 rows if e.g. 1st of current month was on firstweekday - week 5 mostly empty + week 6 empty
            idx = -2
            adn = 2  # rows: cur[-2] <==> other[0]; -1 <==> 1
        for w in range(idx,0):
            wknrs[w] = isowk(datetime.date(next_month.year, next_month.month, other[w+adn][-1]))
            for i in range(7):
                if cur[w][i] == 0:
                    cur[w][i] = other[w+adn][i]
                    fmt[w][i] = STYLE_OTHER
    if 0 in wknrs:
        raise Exception("Week number calculation failed. Week numbers must be > 0.")
    if not all([z==1 for z in [x-y for x,y in zip(wknrs[1:], wknrs)]]):
        if not 52 in wknrs and not 53 in wknrs:
            raise Exception("Week number calculation failed. %s is not increasing homogenously!"%wknrs)
    return (cur, fmt, wknrs)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='fill a template svg calendar with actual dates and week numbers')
    parser.add_argument('sdate', metavar='STARTDAY', nargs='?',
                        default="today",
                        help='start of 3-day running "week". Default="today". Other values: tom, tomorrow, or any date in <year>-<month>-<day> format.')
    args = parser.parse_args()

    today = datetime.datetime.today()

    if args.sdate == "today":
        sdate = today
    elif args.sdate in ["tom", "tomorrow"]:
        sdate = today+datetime.timedelta(1)
    else:
        try:
            sdate = datetime.datetime.strptime(args.sdate, "%Y-%m-%d")
        except Exception as e:
            print("Invalid date format for STARTDAY: %s\n"%e)
            parser.print_help()
            sys.exit(1)

    days, formats, weeknrs = get_monthdata(sdate)

    dlist = list(itertools.chain.from_iterable(days))
    flist = list(itertools.chain.from_iterable(formats))

    # Make them ready to pop() off in correct order
    dlist.reverse()
    flist.reverse()
    weeknrs.reverse()

    with open(TMPLFILE) as t:
        for line in t.readlines():
            if line_re_w.match(line):
                line = re.sub('>w[0-9]<', ">%i<"%weeknrs.pop(), line)
                line = re.sub('style=".*"', 'style="%s"'%STYLE_WKNR, line)
            elif line_re_d.match(line):
                line = re.sub('>[a-vx-z][0-9]<', ">%i<"%dlist.pop(), line)
                line = re.sub('style=".*"', 'style="%s"'%flist.pop(), line)
            elif "§month_year§" in line:
                line = line.replace("§month_year§", "%s %s"%(MONTH_NAMES[sdate.month], sdate.year))
            print(line.rstrip())

