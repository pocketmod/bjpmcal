# Usage

Run as e.g.
```bash
bjpmcal.py > test.svg
```
You can then render the resulting svg file using inkscape:
```bash
inkscape -f test.svg -A test.pdf
```

# License

The main program, bjpmcal.py, is released under GNU General Public License.
For more information see the header included in the file and the file COPYING.

The template file a7tmpl.svg is released under CC0 1.0 Universal license.
For more information see the file CC0.
